# About
All requirements here are directly from Larry from various **previous** sources.

Order of these are original per reference, but order could likely mean very little in this document.

**Note**: headings are used as links in other documents, modify carefully. 

## Definitions:
SM: Slack Message

# Meta-Requirement:
"If both EncycloReader and EncycloSearch can’t effectively implement some network requirement, then that’s a problem, and to that extent, the network isn’t yet decentralized (a key goal)."[^Slack3]

# Requirements
## SM-1
Prefer something easier-to-use to something harder-to-use.[^Slack1]

## SM-2
Stable[^Slack1]

## SM-3
Well-supported.[^Slack1]

## SM-4
Decentralized[^Slack1]
[^Decentralization]

### SM-4.1
The network cannot be shut down by shutting down any central server, but only by shutting down all the individual, independent nodes that constitute the network.[^Slack1]

### SM-4.2
Nodes are equal. No node controls the other nodes, or even has extra permissions over other nodes.[^Slack1]

## SM-5
Open, libre, and free of charge.[^Slack1]

## SM-6
No supporting/owning corporation could easily shut down the network.[^Slack1]

## SM-7
Neither the network nor the data on it can be controlled by any combination of people who own coins connected to the network.[^Slack1]

## SM-8
Anyone can push to network/seed without asking permission of anyone, just as anyone can publish an RSS feed. This is no guarantee that anyone will use the work.[^Slack2]

## SM-9
Aggregators both quickly (constantly and “instantly”) incorporate new changes made to article collections.[^Slack2]

## SM-10
Aggregators make those changes available almost as quickly.[^Slack2]

## SM-11
Aggregators publish whitelists, which should be freely shared across the network. These could apply to both collections and individual articles.[^Slack2]

## SM-12
Aggregators, to be KSF certified, must confirm that articles and collections they represent as the work of some ID have actually been signed by that ID.[^Slack2]

## SM-13
Similarly, KSF-certified aggregators must publish only standards-conforming ZWI (and other?) files.[^Slack2]

## SM-14
The latter means that aggregators will have to enforce standards and reject non-compliant submissions from publishers.[^Slack2]

## SM-15
Aggregators can and should be set up to exchange and compare/reconcile data files, but probably won’t have to do so. A widely-whitelisted publisher should be able to expect their data to be propagated across the network if they upload it to any one aggregator (who whitelists the collection).[^Slack2]

## SM-16
While IDs and ZWIs and other data should be published by publishers (or their temporary proxies) with authoritative sources URLs, aggregators should be able to accept updates from new URLs as long as the signatures are valid.[^Slack2]

## SM-17
At least two encyclopedia publishers are publishing their own signed (and otherwise more-or-less minimally complete) ZWI files to a P2P data storage and exchange network[^Slack4]

## SM-17.1
P2P Network constantly updated by the publishers with the latest versions (not just occasionally) of articles.[^Slack4]

## SM-17.1.1
P2P Network shall version control articles.[^Slack4]

## SM-17.2
At least two orgs (so, one in addition to the KSF) have “complete” (some exclusions are OK) versions of the network’s data available for search and reading.[^Slack4]

## SM-17.3
There must be at least two aggregators in operation, preferably but not necessarily run by different orgs.[^Slack4]
	
## References:
[^Slack1]: [Larry Sanger](@globewalldesk): [Slack message](https://encyclosphere.slack.com/archives/CQ5C1EDV3/p1640874930149000), Dec. 30, 2021.

[^Slack2]: [Larry Sanger](@globewalldesk): [Slack message](https://encyclosphere.slack.com/archives/CQ5C1EDV3/p1643164874126259), Jan. 25, 2022.

[^Slack3]: [Larry Sanger](@globewalldesk): [Slack message](https://encyclosphere.slack.com/archives/CQ5C1EDV3/p1643165376563499), Jan. 25, 2022.

[^Slack4]: [Larry Sanger](@globewalldesk): [Slack message](https://encyclosphere.slack.com/archives/CQ5C1EDV3/p1641866661103100?thread_ts=1641863647.091200&cid=CQ5C1EDV3), Jan 10, 2022.

[^Decentralization]: [Larry Sanger](@globewalldesk): [larrysanger.org](https://larrysanger.org/2021/01/what-decentralization-requires/), Jan. 13, 2021