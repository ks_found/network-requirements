# TODO:
- Bring in larry.md, sort it, debuplicate if needed, build off it, append to it.
- Requirement name prefix?

# Proposed Requirements
# R-PROP-1
- Any scrapped or otherwise backed up content shall be able but not required to provide a mechanism for the original author to be able to stake some form of verifiable authorship claim on that content **after** the backup task completed. [US-Agg-1](user-stories.md#US-Agg-1)
	> Reason being, that if this is decentralized, authors will have no central entity to reconcile reference disputes/inadequacies with.
	This could for instance possibly be as simple as pointing to their DID:web, whether they have one at that time or not, thereby letting them add one latter.
	
# Requirements
## EX_R1 (example)
An important requirement [SM-1](larry.md#sm-1)