# Files
- [larry.md](larry.md), Things Larry put out before this repo.
- [milestones.md](milestones.md), A potential place we can sort requirements into their respective implementations.
- [**requirements.md**](requirements.md), Where the top level of requirements go.
- [user-stories.md](user-stories.md), A place we can document the needs of each type of user, to ground what requirements are created.

# Contributing
1. Try to keep commits short and traceable. This is especially important with any reorganization!
1. Mind links between files.
1. Follow a standard commit message style ([here](#recommended-commit-messages)), if feasible and applicable.
1. Minimally/Don't edit intent of base requirments
   
### Recommended commit messages:
Message|Description
-------|-----------
\+Requirement(s)|Adding any requirement(s)
\-Requirement(s)|Removing any requirement(s)
Reorg(anize)|Reorganizing requirements (No edits, deletions or additions!)
Reword|Rewording existing requirements (in place!)

**Optionally**: append ":\<name>" to commit message if doing something on someone else's behalf.