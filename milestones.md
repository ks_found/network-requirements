# Rev #
## SM-1
Support for DID:PSQR, including actual DIDs for EncycloReader and EncycloSearch, and re-generating signed versions of all articles.[^Slack1]

## SM-2
Updating EncycloCrawler (and the like) to get new versions of articles, and updating the ZWIs successfully, automatically.[^Slack1]

## SM-3
Decide between DAT (or its successor), IPFS, and BitTorrent, or something else, as a fairly permanent preferred network for our software to use for decentralized data storage and transfer.[^Slack1]

## SM-4
Creating either bespoke software for one particular encyclopedia publisher (probably Ballotpedia or Citizendium), enabling them to produce their own ZWIs, sign them, and push them to the network; OR creating a MediaWiki plugin that either could use in this way. We will want to do the latter eventually.[^Slack1]

# Rev 0
example only (TODO)

# Rev 1
...

[^Slack1]: [Larry Sanger](@globewalldesk): [Slack message](https://encyclosphere.slack.com/archives/CQ5C1EDV3/p1641863647091200), Jan 10, 2022.